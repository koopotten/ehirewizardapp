//
//  FirstViewController.swift
//  EHireWizardApp
//
//  Created by Koop on 10/09/15.
//  Copyright (c) 2015 Koop. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    @IBOutlet weak var loanSwitch: UISwitch!
    @IBOutlet weak var loanQuestionLabel: UILabel!
    @IBOutlet weak var hourlyWageContainer: UIView!
    
    @IBOutlet weak var hourlyRateMultiplier: UILabel!
    @IBOutlet weak var hourlyRateLabel: UILabel!
    @IBOutlet weak var hourlyRateField: UITextField!
    @IBOutlet weak var hourlyRateOutput: UITextView!
    
    @IBOutlet weak var SixTeenYearLabel: UILabel!
    @IBOutlet weak var SixTeenTextField: UITextField!
    @IBOutlet weak var SixTeenMultiplier: UILabel!
    @IBOutlet weak var SixTeenOutput: UITextView!
    
    @IBOutlet weak var SevenTeenYearLabel: UILabel!
    @IBOutlet weak var SevenTeenTextField: UITextField!
    @IBOutlet weak var SevenTeenMultiplier: UILabel!
    @IBOutlet weak var SevenTeenOutput: UITextView!
    
    @IBOutlet weak var nextBtn: UIButton!
    
    @IBOutlet weak var TextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupLabels()
        setupSelectors()
        changeStateOfAgeSpecificFields(true)
//        self.view.backgroundColor = UIColor(hexString:"#f77f00")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLabels() {
        loanQuestionLabel.text = "Zijn de lonen leeftijd specifiek?"
        hourlyRateLabel.text = "Vast uurtarief:"
        
        hourlyRateOutput.layer.borderWidth = 1.0
        hourlyRateOutput.layer.borderColor = UIColor.greenColor().CGColor
        
        SixTeenOutput.layer.borderWidth = 1.0
        SixTeenOutput.layer.borderColor = UIColor.greenColor().CGColor
        
        SevenTeenOutput.layer.borderWidth = 1.0
        SevenTeenOutput.layer.borderColor = UIColor.greenColor().CGColor
        nextBtn.layer.backgroundColor = UIColor.orangeColor().CGColor
    }
    
    func setupSelectors() {
        loanSwitch.addTarget(self, action: Selector("stateChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func stateChanged(switchState: UISwitch) {
        if switchState.on {
            changeStateOfHourlyWageFields(true)
            changeStateOfAgeSpecificFields(false)
        } else {
            changeStateOfHourlyWageFields(false)
            changeStateOfAgeSpecificFields(true)
        }
    }
    
    func changeStateOfHourlyWageFields(hidden : Bool) {
        hourlyRateLabel.hidden = hidden
        hourlyRateField.hidden = hidden
        hourlyRateOutput.hidden = hidden
        hourlyRateMultiplier.hidden = hidden
    }
    
    func changeStateOfAgeSpecificFields(hidden: Bool) {
        SixTeenYearLabel.hidden = hidden
        SixTeenTextField.hidden = hidden
        SixTeenMultiplier.hidden = hidden
        SixTeenOutput.hidden = hidden
        SevenTeenYearLabel.hidden = hidden
        SevenTeenTextField.hidden = hidden
        SevenTeenMultiplier.hidden = hidden
        SevenTeenOutput.hidden = hidden
    }
    
    @IBAction func hourlyRateTextFieldChange(sender: AnyObject) {
        if hourlyRateField.text != "" {
            var hourlyRate = Float(hourlyRateField.text.toInt()!)
            var tariff : Float = Calculator().calculateTariff(hourlyRate)
            hourlyRateOutput.text = String(stringInterpolationSegment: tariff)
        }
    }
    
    @IBAction func SixTeenTextField(sender: AnyObject) {
        if SixTeenTextField.text != "" {
            var hourlyRate = Float(SixTeenTextField.text.toInt()!)
            var tariff : Float = Calculator().calculateTariff(hourlyRate)
            SixTeenOutput.text = String(stringInterpolationSegment: tariff)
        }
    }
    
    @IBAction func SevenTeenTextField(sender: AnyObject) {
        if SevenTeenTextField.text != "" {
            var hourlyRate = Float(SevenTeenTextField.text.toInt()!)
            var tariff : Float = Calculator().calculateTariff(hourlyRate)
            SevenTeenOutput.text = String(stringInterpolationSegment: tariff)
        }
    }
    
    @IBAction func nextBtnClicked(sender: AnyObject) {
        self.tabBarController!.selectedIndex = 1;
    }
    
}

