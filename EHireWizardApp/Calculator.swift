//
//  Calculator.swift
//  EHireWizardApp
//
//  Created by Koop on 10/09/15.
//  Copyright (c) 2015 Koop. All rights reserved.
//

import Foundation
class Calculator {
    func calculateTariff(hourlyRate : Float) -> Float {
        return Float(hourlyRate * 1.85)
    }
}