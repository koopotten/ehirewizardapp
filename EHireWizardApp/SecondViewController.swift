//
//  SecondViewController.swift
//  EHireWizardApp
//
//  Created by Koop on 10/09/15.
//  Copyright (c) 2015 Koop. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet weak var functionLabel: UILabel!
    @IBOutlet weak var titleOrder: UILabel!
    @IBOutlet weak var functionSelect: UIPickerView!
    @IBOutlet weak var datePickerLabel: UILabel!
    @IBOutlet weak var buttonPreview: UIButton!
    @IBOutlet weak var orderTitle: UITextField!
    @IBOutlet weak var rowSelected: UIPickerView!

    
    var pickerDataSource = ["IT", "Medical", "Agricultural", "Foobar"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = UIColor(hexString:"#f77f00")
        setupLabels()
        setupPreviewButton()
        
        self.functionSelect.dataSource = self
        self.functionSelect.delegate = self
        self.title = "Opdracht toevoegen"

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func setupLabels() {
        datePickerLabel.text = "Datum"
        titleOrder.text = "Opdracht titel"
        functionLabel.text = "Beroepsgroepen"
    }

    
    func numberOfComponentsInPickerView(functionSelect: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(functionSelect: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(functionSelect: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickerDataSource[row]
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 36
    }
    
    @IBAction func clickButton(sender: UIButton) {
        println("buTON!")
        println(orderTitle.text)
        
        var alert = UIAlertController(title: "Voorbeeld Opdracht", message: orderTitle.text + " " + pickerDataSource[1], preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "WAT", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
     
    }
    
    func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 200
    }
   
    func setupPreviewButton() {
        
        buttonPreview.layer.cornerRadius = 5
        buttonPreview.layer.borderWidth = 1
        buttonPreview.layer.borderColor = UIColor.blackColor().CGColor
        buttonPreview.layer.backgroundColor = UIColor.blackColor().CGColor
        buttonPreview.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    }

}

